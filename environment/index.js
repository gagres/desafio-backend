const dotenv = require('dotenv'),
      path   = require('path');

module.exports = () => {
    let filename;
    
    switch(process.env.NODE_ENV) {
        case 'production':
            filename = '.prod.env';
            break;
        default:
            filename = '.dev.env';
    }

    dotenv.config({ path: path.resolve(__dirname, filename) });

}