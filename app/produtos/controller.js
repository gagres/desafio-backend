module.exports = server => {
    const ProdutoModel = server.app.produtos.Produto;

    class ProdutoCtrl {
        getAll(req, res) {
            ProdutoModel
                .getAll()
                .then( produtos => res.json({ data: produtos }) )
                .catch( err => res.status(500).json({ err, message: 'Não foi possível gerar a lista de produtos' }) );
        }
        create(req, res) {
            ProdutoModel
                .create(req.body)
                .then( produto => res.json({ data: 'Produto adicionado com sucesso' }))
                .catch( err => res.status(500).json({ err, message: 'Não foi possível criar o produto desejado' }) );
        }
        update(req, res) {
            const { id } = req.params;

            ProdutoModel
                .update(id, req.body)
                .then( produto => res.json({ data: 'Produto atualizado com sucesso' }) )
                .catch( err => res.status(500).json({ err, message: 'Não foi possível atualizar o produto desejado' }) );
        }
        remove(req, res) {
            const { id } = req.params;

            ProdutoModel
                .remove(id)
                .then( produto => res.json({ data: 'Produto removido com sucesso' }) )
                .catch( err => res.status(500).json({ err, message: 'Não foi possível remover o produto desejado' }) );
        }
    }

    return new ProdutoCtrl();
}