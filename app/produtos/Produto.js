module.exports = server => {

    const connection = server.connection;

    class ProdutoModel {
        getAll() {
            return connection.queryAsync(`
                SELECT id, nome, descricao, imagem, valor, fator, created_at, updated_at
                FROM produto`
            )
        }
        create(produto) {
            return connection.queryAsync(`
                INSERT INTO produto (nome, descricao, imagem, valor, fator, created_at)
                VALUES (:nome, :descricao, :imagem, :valor, :fator, NOW())
            `, produto );
        }
        update(id, produto) {
            return connection.queryAsync(`
                UPDATE produto 
                SET nome       = :nome,
                    descricao  = :descricao,
                    imagem     = :imagem,
                    valor      = :valor,
                    fator      = :fator,
                    updated_at = NOW() 
            `, Object.assign(produto, { id }) );
        }
        remove(id) {
            return connection.queryAsync(`
                DELETE FROM produto WHERE id = :id
            `, { id });
        }
    }

    return new ProdutoModel();
}