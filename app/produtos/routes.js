module.exports = server => {

    const ProdutoCtrl = server.app.produtos.controller;

    server.route('/produtos')
        .get(ProdutoCtrl.getAll)
        .post(ProdutoCtrl.create);

    server.route('/produto/:id')
        .put(ProdutoCtrl.update)
        .delete(ProdutoCtrl.remove);

}