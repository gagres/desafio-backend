const mysql    = require('mysql'),
      bluebird = require('bluebird'); 

module.exports = app => {
    const { DB_HOST, DB_NAME, DB_USER, DB_PASS } = process.env;

    let connection = mysql.createConnection({
        host:     DB_HOST,
        user:     DB_USER,
        password: DB_PASS,
        database: DB_NAME
    });

    // Enable <key, value> scape variables in the query
    connection.config.queryFormat = function (query, values) {
        if (!values) return query;
        return query.replace(/\:(\w+)/g, function (txt, key) {
            if (values.hasOwnProperty(key)) {
                return this.escape(values[key]);
            }
            return txt;
        }.bind(this));
    };

    app.connection = bluebird.promisifyAll(connection); // Convert all sql functions to Promises

    app.connection.queryAsync('SELECT 1 + 1 AS Solution')
        .then(( rows ) => console.info('Database conectado com sucesso'))
        .catch( err => console.error('A conexão com o banco de dados não pode ser estabelecida'));


}