const express    = require('express'),
      bodyParser = require('body-parser'),
      cors       = require('cors'),
      morgan     = require('morgan'),
      path       = require('path');

module.exports = (app) => {

    app.set(express.static(path.resolve(__dirname, 'public'))); // public folder

    app.use(cors()); // Enable cross-origin

    app.use(morgan('combined'));

    // Enable request's body
    app.use(bodyParser.json());
    app.use(bodyParser.urlencoded({ extended: false }));
}