const express     = require('express'),
      consign     = require('consign'),
      environment = require('./environment');

environment(); // Init environment configs

let app = express();

consign()
    .include('configs')
    .then('app')
    .into(app);

app.get('/', (req, res) => res.json({ 'hello': 'world' }));

const server = app.listen(process.env.PORT, () => {
    const port = server.address().port;
    
    console.info(`Aplicação inicializada (${ process.env.NODE_ENV }) - Porta: ${ port }`)
})